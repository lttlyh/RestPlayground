module.exports = {
    port: process.env.VMC_APP_PORT || 18080,
    host: process.env.VCAP_APP_HOST || 'localhost',
    dburl: 'mongodb://localhost:27017/testdb'
}
