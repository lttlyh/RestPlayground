var _ = require('underscore');
var debug = require('debug')('restplayground');

var config = require('./config.js');
global.config = config = _.extend({
    port: 3000,
    host: 'localhost'
}, config);

var db = require('./lib/db.js');
var app = require('./app.js');

db.connect(function(){
    var server = app.listen(config.port, config.host, function(){
	var address = server.address();
	debug('Express server listening on port ' + address.port + ' at ' + address.address);
    });
});
