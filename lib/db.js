var mongo = require('mongodb');
var MongoClient = mongo.MongoClient;

var db = {};
var database = null;

db.connect = function connect(cb){
    MongoClient.connect(global.config.dburl, function(err, db){
	if(err)
	    throw err;
	database = db;
	cb();
    });
};

db.getDb = function(){
    return database;
}

module.exports = db;
